const mongoose = require("mongoose");

const { Schema } = mongoose;

const ProductsSchema = new Schema({
  name: String,
  price: Number,
  weight: Number,
  img_url: String,
  status: String,
  discount: Number,
  detail: Array
}, { timestamps: true }
);

ProductsSchema.methods.toJSON = function() {
  return {
    _id: this._id,
    name: this.name,
    price: this.price,
    img_url: this.img_url,
    status: this.status,
    discount: this.discount,
    detail: this.detail
  };
};

ProductsSchema.methods.addToCart = function() {
  const hash = crypto
    .pbkdf2Sync(password, this.salt, 10000, 512, "sha512")
    .toString("hex");
  return this.hash === hash;
};

mongoose.model("Products", ProductsSchema);
