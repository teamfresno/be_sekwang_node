const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
// const Products = mongoose.model("Products");

const { Schema } = mongoose;

const TransactionsSchema = new Schema(
  {
    user_id: { type: Schema.Types.ObjectId, ref: "Users" },
    items: [{
      product_id: { type: Schema.Types.ObjectId, ref: "Products"},
      qt : Number,
      _id: false
    }],
    status: String
  },
  { minimize: false, timestamps: true }
);

TransactionsSchema.methods.toJSON = function() {
  return {
    _id: this._id,
    user_id: this.user_id,
    items: this.items,
    status: this.status
  };
};

TransactionsSchema.plugin(mongoosePaginate);

mongoose.model("Transactions", TransactionsSchema);
