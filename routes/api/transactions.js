const mongoose = require("mongoose");
const router = require("express").Router();
const auth = require("../auth");
const Transactions = mongoose.model("Transactions");

//GET a transaction route (required, authorized user has access)
router.get("/", auth.required, (req, res, next) => {
  const {
    payload: { id }
  } = req;

  return Transactions.findOne({ user_id: id, status: "active" })
    .populate("items.product_id")
    .then(cart => {
      if (!cart) {
        const newCart = {
          user_id: id,
          items: [],
          status: "active"
        };

        const finalCart = new Transactions(newCart);

        return finalCart
          .save()
          .then(() => res.json({ cart: finalCart.toJSON() }));
      }
      return res.json({ cart: cart.toJSON() });
    });
});

//GET all transactions route (required, authorized user has access)
router.get("/all", auth.required, (req, res, next) => {
  const {
    payload: { id }
  } = req;

  return Transactions.find({ user_id: id }).then(transactions => {
    if (!transactions) {
      return res.sendStatus(400);
    }
    return res.json({ transactions });
  });
});

//POST add product to a cart (required, authorized user has access)
router.post("/cart", auth.required, (req, res, next) => {
  const {
    payload: { id },
    body: { product }
  } = req;

  if (typeof product.qt !== "number" || product.qt >= 100) {
    return res.sendStatus(400);
  }

  return Transactions.findOneAndUpdate(
<<<<<<< HEAD
    { user_id: id, status: "active", "items.product_id": product.product_id },
    { $set: { "items.$.qt": product.qt } },
    { new: true }
  )
    .then(updatedCart => {
      if (!updatedCart) {
        return Transactions.findOneAndUpdate(
          { user_id: id, status: "active" },
          { $push: { items: product } },
          { new: true }
        ).then(itemPushedCart => {
          return res.json({ cart: itemPushedCart });
        });
=======
      { user_id: id, status: "active", "items.product_id": product.product_id },
      { $set: { "items.$.qt": product.qt } },
      { new: true }
    )
    .then(updatedCart => {
      if (!updatedCart) {
        return Transactions.findOneAndUpdate(
            { user_id: id, status: "active" },
            { $push: { items: product } },
            { new: true }
          )
          .then(itemPushedCart => {
            return res.json({ cart: itemPushedCart });
          })
          .catch(err => {
            return res.sendStatus(400);
          });
>>>>>>> d29010e9111702b61566bd2a0a5b56729352aec9
      } else {
        return res.json({ cart: updatedCart });
      }
    })
    .catch(err => {
      return res.sendStatus(400);
    });
});

//POST delete selected item from cart (required, authorized user has access)
router.post("/cart/delete", auth.required, (req, res, next) => {
  const {
    payload: { id },
    body: { product }
  } = req;

  return Transactions.findOneAndUpdate(
      { user_id: id, status: "active" },
      { $pull: { items: { product_id: product.product_id } } },
      { new: true }
    ).then(updatedCart => {
      if (!updatedCart) {
        return res.sendStatus(400);
      } else {
        return res.json({ cart: updatedCart });
      }
    })
    .catch(err => {
      return res.sendStatus(400);
    })
});

// ----------------- admin apis --------------------------
//GET transactions route (admin required, only admin has access)
router.get("/admin", auth.required, (req, res, next) => {
  const {
    body: { query }
  } = req;

  const options = {
    page: query.page ? query.page : 1,
    limit: 10
  };

  return Transactions.paginate(
    {
      status: query.status ? query.status : ["active", "complete"], // list of status
      user_id: query.user_id ? query.user_id : { $exists: true },
      updatedAt: {
        $gte: new Date(query.year_from, query.month_from, query.day_from),
        $lte: new Date(query.year_to, query.month_to, query.day_to)
      }
    },
    options
  )
    .then(transactions => {
      if (!transactions) {
        res.sendStatus(400);
      }
      return res.json({ transactions: transactions });
    })
    .catch(error => {
      res.sendStatus(400);
    });
});

// PUT transactions route (admin required, only admin has access)
router.put("/modify", auth.required, (req, res, next) => {
  const {
    body: { transaction }
  } = req;

  return Transactions.updateOne(
    {
      _id: transaction._id
    },
    transaction,
    { timestamps: true }
  )
    .then(doc => {
      if (!doc) {
        res.sendStatus(400);
      }
      return res.json({ transaction: transaction });
    })
    .catch(error => {
      res.sendStatus(400);
    });
});

module.exports = router;
