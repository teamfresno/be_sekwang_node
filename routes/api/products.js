const mongoose = require("mongoose");
const router = require("express").Router();
const auth = require("../auth");
const Products = mongoose.model("Products");

//GET all products route (optional, everyone has access)
router.get("/", auth.optional, (req, res, next) => {
  return Products.find().then(products => {
    if (!products) {
      res.sendStatus(400);
    }
    return res.json({ products: products });
  });
});

//GET a product detail route (optional, everyone has access)
router.get("/:id", auth.optional, (req, res, next) => {
  return Products.findById(req.params.id).then(product => {
    if (!product) {
      res.sendStatus(400);
    }
    return res.json({ product: product.toJSON() });
  });
});

//POST a new product route (admin required, only admin has access)
router.post("/new_product", auth.required, (req, res, next) => {
  const {
    body: { product }
  } = req;

  if (
    !product.name ||
    !product.price ||
    !product.weight ||
    !product.img_url ||
    !product.discount ||
    !product.status
  ) {
    return res.status(422).json({
      errors: {
        data: "is invalid"
      }
    });
  }

  const finalProduct = new Products(product);

  return finalProduct
    .save()
    .then(() => res.json({ product: finalProduct.toJSON() }));
});

//PUT update a product route (admin required, only admin has access)
router.put("/", auth.optional, (req, res, next) => {
  const {
    body: { product }
  } = req;

  if (
    !product._id ||
    !product.name ||
    !product.price ||
    !product.weight ||
    !product.img_url ||
    !product.discount ||
    !product.status
  ) {
    return res.status(422).json({
      errors: {
        data: "is invalid"
      }
    });
  }

  const query = { _id: product._id };

  return Products.findOneAndUpdate(query, product, { new: true }).then(
    product => {
      if (!product) {
        res.sendStatus(400);
      }
      return res.json({ product: product.toJSON() });
    }
  );
});

module.exports = router;
